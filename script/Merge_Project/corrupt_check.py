from struct import unpack
import os


marker_mapping = {
    0xffd8: "Start of Image",
    0xffe0: "Application Default Header",
    0xffdb: "Quantization Table",
    0xffc0: "Start of Frame",
    0xffc4: "Define Huffman Table",
    0xffda: "Start of Scan",
    0xffd9: "End of Image"
}


class JPEG:
    def __init__(self, image_file):
        with open(image_file, 'rb') as f:
            self.img_data = f.read()
    
    def decode(self):
        data = self.img_data
        while(True):
            marker, = unpack(">H", data[0:2])
            # print(marker_mapping.get(marker))
            if marker == 0xffd8:
                data = data[2:]
            elif marker == 0xffd9:
                return
            elif marker == 0xffda:
                data = data[-2:]
            else:
                lenchunk, = unpack(">H", data[2:4])
                data = data[2+lenchunk:]            
            if len(data)==0:
                break        





def IsCorruptedImage(image):
    '''
        description : check if folder has corrupted image
        imput : folder path
    '''
    try:

      JpgImage = os.path.join(image)#osp.join(root_img,img)
      img = JPEG(JpgImage) 

      try:
        img.decode()
        return True 
      except Exception as msg:
        print('----',msg)
        print('Corrupted image found {}'.format(JpgImage))
        os.remove(os.path.join(JpgImage))
        print('{} deleted'.format(JpgImage))
        return False

    except Exception as msg:
        print(msg)
        return False


if __name__ == "__main__":
    base='/home/gaming/Downloads/Segregation/tf_1_training/mask-rcnn/batch1/turfarea/test_images'
    for jpg_file in os.listdir(base):
        IsCorruptedImage(os.path.join(base,jpg_file))