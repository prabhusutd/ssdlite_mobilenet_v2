import os , shutil , time , traceback
from os.path import join as JoinPath
from os.path import isfile 
from pathlib import Path
from glob import glob
import pandas as pd
import zipfile  , py7zr
from datetime import datetime
import pyfastcopy
from tqdm import tqdm
import tarfile


from vott_create_pascal_tf_record import main as make_tf_record
from create_label_map import main as make_label_pbtxt
from configure_pipeline import main as config_pipeline
import config
from corrupt_check import IsCorruptedImage

base_dir = config.MERGE_CONFIG['base_dir']


dest_dir = config.MERGE_CONFIG['dest_dir']
dest_dir = JoinPath(dest_dir,'Target')

dirs = ['TRAIN','TEST']
sub_dir = [JoinPath('Annotation','JPEGImages') , JoinPath('ImageSets','Main')]

#assigning multiple empty list 
xml_list,jpg_list,tag_name_list,tag_data_list,filename_list,img_present_flag=([] for _ in range(6))

#dictionary for text file
report_dictionary={'Title':'','Date and Time':'','No of Classes':0,'Batch Size':0,'No of Steps':0,
                    'Train Tf Record Path':'','Test Tf Record Path':'' , 'Train Pbtxt Path':'',
                    'Test Pbtxt Path':'','Pipeline Config Path':'' , 'No of Images in Train Record':'',
                    'No of Images in Test Record':'','Source Filenames':[]}


def timer(func):
    def wrapper(*args,**kwargs):
        start=time.time()
        ret = func(*args,**kwargs)
        end = time.time() - start
        print(f'Time taken to run {func.__name__} {end}')
        return ret
    return wrapper

@timer
def unzip_files(base_dir):
    '''
        description : iterate through base directory check for zip files and extract 
                      in base directory
        support : 7z and zip
    '''
    try:
        for file in tqdm(os.listdir(base_dir)):

            if file.endswith('.7z'):
                with py7zr.SevenZipFile(JoinPath(base_dir,file), mode='r') as zip_ref:
                    zip_ref.extractall(path=JoinPath(base_dir,file.split('.')[0]))
                    report_dictionary['Source Filenames'].append(file)

            if file.endswith('.zip'):
                with zipfile.ZipFile(JoinPath(base_dir,file), 'r') as zip_ref:
                    zip_ref.extractall(JoinPath(base_dir))
                    report_dictionary['Source Filenames'].append(file)

            if file.endswith('tar.gz'):
                TarFile = tarfile.open(JoinPath(base_dir,file))
                # extracting file
                TarFile.extractall(JoinPath(base_dir))
                report_dictionary['Source Filenames'].append(file)
                TarFile.close()
                
        return True
        
    except Exception as msg:
        traceback.print_exc()
        return False


@timer
def create_dest_folder_structure(dest_dir):
    '''
        description: create training folders and annotation folders in destionation
        return : boolean
    '''
    #creates Annotation/JPEGImages , ImageSets/Main folder structure 
    try :  
        [Path(JoinPath(dest_dir,dir,sub)).mkdir(parents=True, exist_ok=True) for sub in sub_dir for dir in dirs]

        Path(JoinPath(dest_dir,'output')).mkdir(parents=True,exist_ok=True)
        Path(JoinPath(dest_dir,'training')).mkdir(parents=True,exist_ok=True)

        return True
    except  Exception as msg:
        traceback.print_exc()
        return False
@timer
def SplitDataset(dataframe,train_percent):
    '''
        description : split the dataframes into train and test
        input : dataframe and training percentage
        return : dataframe
    '''
    print(f'Spliting datasets into train {round((train_percent)*100)} percentage and test {round((1-train_percent)*100)} percentage')
    train = dataframe.sample(frac=train_percent,random_state=200)
    test = dataframe.drop(train.index)
    return train , test


@timer
def main(base_dir,dest_dir):
    '''
       description: iterates through base directory and copy files to destination \
                    folder with train and test splits
        input : base direactory and destination directory
    '''
    try:
        
        for directory in tqdm(os.listdir(base_dir)):
            cur_base_dir =JoinPath(base_dir,directory)

            if os.path.isdir(cur_base_dir): 
                 #iterate  all directory 
                for root , dir, file in os.walk(cur_base_dir):
                    #Assume Annotation folder will have jpegimages and imagesets folder
                    if  'Annotations' in dir: 
                        ann_fol= list(Path(root).glob('**/Annotations'))[0]
                        jpg_fol = list(Path(root).glob('**/JPEGImages'))[0]
                        tag_fol = list(Path(root).glob('**/Main'))[0]
                        
                        #iterate tag files 
                        for tag in os.listdir(tag_fol):
                            tag_file=open(JoinPath(tag_fol, tag), 'r')   
                            tag_data= [line for line in tag_file.readlines()]

                            for name in tag_data:
                                file_name   = name.split(' ')[0]  
                                ImgFlag = name.split(' ')[1].split('\n')[0]


                                xml_check = isfile(JoinPath(ann_fol,file_name.split('.')[0]+'.xml')) 

                                jpg_check = isfile(JoinPath(jpg_fol,file_name)) 



                                #verify if xml ,jpg files are present and entry all data into lists
                                

                                if xml_check == True and jpg_check == True :
                                    

                                    xml_list.append(JoinPath(ann_fol,file_name.split('.')[0]+'.xml'))
                                    jpg_list.append(JoinPath(jpg_fol,file_name))
                                    tag_name_list.append(tag)
                                    tag_data_list.append(name)
                                    filename_list.append(file_name)
                                    img_present_flag.append(ImgFlag)

                                    
                                if not xml_check and jpg_check :
                                    print('file missing name = {} xml = {} jpg_check = {} iscorrupted ={} '.format(name,xml_check,jpg_check))

        #create dictionary with data lists
        check_file = {'Tag Name':tag_name_list,'Tag Data':tag_data_list , 'XML Path':xml_list , 'JPG Path':jpg_list,
                        'File Name':filename_list , 'Img Flag':img_present_flag}

        files_dataframe = pd.DataFrame(check_file)
        #print(files_dataframe.count())
        
        files_dataframe = files_dataframe[files_dataframe['Img Flag'] != '-1'] #remove all rows that as negative flag(represents image not annotated)
        
        
        
        
        #print(files_dataframe.count(),files_dataframe['File Name'].nunique())
        #print(len([i for  i in files_dataframe.iterrows()]))

        #pd.concat(g for _, g in files_dataframe.groupby("File Name") if len(g) > 1).to_csv('Duplicated.csv')
        #print(files_dataframe)

        
        
        train,test = SplitDataset(files_dataframe,train_percent=0.8) #splite dataframe into two 80 : 20 ratio 

        
        #print(train.count(),test.count(),train['File Name'].nunique(),test['File Name'].nunique())
        #print(train.groupby('Tag Name').count())
        #print(len(train),len(test))

        #create a destination folder
        print('Creating Folder Structure Done' if create_dest_folder_structure(dest_dir) else 'Folder Structure Creation Failed')
            

        #write csv file
        files_dataframe.to_csv(JoinPath(dest_dir,'OUTPUT_'+str(int(time.time()))+'.csv'),index=False)
        print('All Required Data Wrapped into CSV')


        #iterate the dataframe and copy the files to associated directory
        for dir in dirs:
            print('Moving Files to {} Folder'.format(dir))

            if dir == 'TRAIN' :
                dataset = train.iterrows()
                
            elif dir == 'TEST':
                dataset = test.iterrows()
                
            else :
                dataset = None

            if dataset is not None:
                
                for index, row in tqdm(dataset):
                    
                    #print(row['Tag Name'],row['Tag Data'], row['XML Path'],row['JPG Path'],row['File Name'])
                    with open(JoinPath(dest_dir,dir,'ImageSets','Main',row['Tag Name']) , 'a+') as tag_file:
                       if '\n' not in row['Tag Data']:
                           row['Tag Data']=row['Tag Data']+'\n'
                       tag_file.write(row['Tag Data'])

                    if not os.path.exists(JoinPath(dest_dir,dir,'Annotation','JPEGImages',row['File Name'])):                    
                        shutil.copy(JoinPath(row['JPG Path']),
                            JoinPath(dest_dir,dir,'Annotation','JPEGImages',row['File Name']))
                        shutil.copy(JoinPath(row['XML Path']),
                            JoinPath(dest_dir,dir,'Annotation',row['File Name'].split('.')[0]+'.xml'))
                    else:
                        #print('file exists')
                        pass
                
            else :
                print('dataset is None')

        target_dirs , tags = [JoinPath(dest_dir,dir) for dir in dirs] , [tag.split('.')[0] for tag in (set(tag_name_list))]
        

        return target_dirs , tags
    except Exception as msg:
        print(msg)
        traceback.print_exc()
        pass
    

    
if __name__ == '__main__':   
    
    
    try:
        zip_resp=unzip_files(base_dir)#unzip all zip files

        dest_pipeline=JoinPath(dest_dir,'pipeline.config')
        train_file='/home/gaming/DIC_training/Tensorflow_1/models/research/object_detection/model_main.py'

        if zip_resp:
            print('Files Unzipped Successfully')
            record_dirs , tag_list =main(base_dir,dest_dir) #merge the unzipped folders


            pbtxt_paths=[make_label_pbtxt(tag_list,dir) for dir in record_dirs] #write pbtxt files

            for dir in record_dirs:
                print('validating each file ')
                for file in tqdm(os.listdir(JoinPath(dest_dir,dir,'Annotation','JPEGImages'))):
                    IsCorruptedImage(JoinPath(dest_dir,dir,'Annotation','JPEGImages',file))


            records_path=[make_tf_record(dir,dir.split('/')[-1]+'.record') for dir in record_dirs] #create tf records

            config_parameters={'src_pipeline_fname':config.MERGE_CONFIG['src_pipeline_fname'],
                                'dest_pipeline_fname':dest_pipeline,'label_map_pbtxt_fname':pbtxt_paths[0],
                                'train_record_fname':records_path[0][0],'test_record_fname':records_path[1][0],
                                'batch_size':config.MERGE_CONFIG['batch_size'],'num_steps':config.MERGE_CONFIG['num_steps'],
                                'num_classes':len(tag_list),'num_examples':records_path[1][1]}

            #edit and save piepline.config file
            if(config_pipeline(**config_parameters)):
                print('pipeline config saved')
                print('\n')
                train_command = 'python {} --pipeline_config_path  {}  --model_dir {}  --alsologtostderr --num_train_steps {} --use_gpu True --run_once True'.format(train_file,dest_pipeline,JoinPath(dest_dir,'training'),config.MERGE_CONFIG['num_steps']) 
                print(train_command)
                print('\n')
                print('tensorboard --logdir {}'.format(JoinPath(dest_dir,'training')))

            
        
        report_dictionary['Title']='{}_{}_{}_{}'.format(config.MERGE_CONFIG['num_steps'] ,
                                    config.MERGE_CONFIG['batch_size'],len(tag_list),
                                    (records_path[0][1]+records_path[1][1]))
        report_dictionary['Date and Time'] = str(datetime.now())
        report_dictionary['Batch Size'] = config.MERGE_CONFIG['batch_size']   
        report_dictionary['No of Steps'] = config.MERGE_CONFIG['num_steps'] 
        report_dictionary['No of Classes'] = len(tag_list)  
        report_dictionary['Pipeline Config Path'] = dest_pipeline
        report_dictionary['Train Tf Record Path'] = records_path[0][0]
        report_dictionary['Test Tf Record Path'] = records_path[1][0]
        report_dictionary['No of Images in Train Record'] = records_path[0][1]
        report_dictionary['No of Images in Test Record'] = records_path[1][1]
        report_dictionary['Train Pbtxt Path'] = pbtxt_paths[0]
        report_dictionary['Test Pbtxt Path'] = pbtxt_paths[1]

        #print(report_dictionary)
        with open(JoinPath(dest_dir,'report_'+str(int(time.time()))+'.txt'),'w') as f:
            f.write(str(report_dictionary).replace("'","").replace('{','').replace('}','').replace(',','\n'))
     
    except Exception as msg:
        #traceback.print_exc()
        print('main ',msg)
        pass
    
    
    


    





