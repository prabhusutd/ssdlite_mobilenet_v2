
MERGE_CONFIG = {
	'base_dir' : '/content/ssdlite_mobilenet_v2/road-kerb-scupper-hole/source',
	'dest_dir' : '/content/ssdlite_mobilenet/road-kerb-scupper-hole/',
    'src_pipeline_fname':'/content/ssdlite_mobilenet_v2/script/Merge_Project/pipeline.config',
    'batch_size':24,
    'num_steps' : 100000,
}
