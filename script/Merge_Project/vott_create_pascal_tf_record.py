

"""Convert raw PASCAL dataset to TFRecord for object_detection.

Example usage:
    python object_detection/dataset_tools/create_pascal_tf_record.py \
        --data_dir=/home/user/VOCdevkit \
        --year=VOC2012 \
        --output_path=/home/user/pascal.record

python  /home/gaming/DIC_training/TensorFlow/scripts/create_pascal_tf_record.py \
  --data_dir= /home/gaming/DIC_training/vott_target/CTM-PascalVOC-export \
        --year=merged \
        --output_path=/home/gaming/DIC_training/vott_target/CTM-PascalVOC-export/pascal.record \
        --label_map_path=/home/gaming/DIC_training/vott_target/CTM-PascalVOC-export/pascal_label_map.pbtxt

python  /home/gaming/DIC_training/TensorFlow/scripts/create_pascal_tf_record.py --data_dir= /home/gaming/DIC_training/vott_target/CTM-PascalVOC-export --year=merged  --output_path=/home/gaming/DIC_training/vott_target/CTM-PascalVOC-export/pascal.record  --label_map_path=/home/gaming/DIC_training/vott_target/CTM-PascalVOC-export/pascal_label_map.pbtxt

python  /home/gaming/DIC_training/TensorFlow/scripts/create_pascal_tf_record.py --data_dir= /home/gaming/DIC_training/polygon_vott_labelled_2000/bundle/train/ --year=merged  --output_path=/home/gaming/DIC_training/polygon_vott_labelled_2000/bundle/train/train.record  --label_map_path=/home/gaming/DIC_training/polygon_vott_labelled_2000/bundle/train/pascal_label_map.pbtxt

python  /home/gaming/DIC_training/TensorFlow/scripts/create_pascal_tf_record.py --data_dir= /home/gaming/DIC_training/polygon_vott_labelled_2000/bundle/test/ --year=merged  --output_path=/home/gaming/DIC_training/polygon_vott_labelled_2000/bundle/test/test.record  --label_map_path=/home/gaming/DIC_training/polygon_vott_labelled_2000/bundle/test/pascal_label_map.pbtxt
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import hashlib
import io
import logging
import os
from os.path import join as JoinPath

from lxml import etree
import PIL.Image
import tensorflow.compat.v1 as tf

import traceback

import xml.etree.ElementTree as ET
from object_detection.utils import dataset_util
from object_detection.utils import label_map_util

'''
flags = tf.app.flags
flags.DEFINE_string('data_dir', '', 'Root directory to raw PASCAL VOC dataset.')
flags.DEFINE_string('set', 'train', 'Convert training set, validation set or '
                    'merged set.')
flags.DEFINE_string('annotations_dir', 'Annotation',
                    '(Relative) path to annotations directory.')
flags.DEFINE_string('year', 'VOC2007', 'Desired challenge year.')
flags.DEFINE_string('output_path', '', 'Path to output TFRecord')
flags.DEFINE_string('label_map_path', 'data/pascal_label_map.pbtxt',
                    'Path to label map proto')
flags.DEFINE_boolean('ignore_difficult_instances', False, 'Whether to ignore '
                     'difficult instances')
FLAGS = flags.FLAGS
'''

#SETS = ['train', 'val', 'trainval', 'test']
#YEARS = ['VOC2007', 'VOC2012', 'merged']


#directory='/home/gaming/Downloads/Segregation/check/Batch1_turf_area_training/Target/TRAIN'

def dict_to_tf_example(data,
                       dataset_directory,
                       label_map_dict,
                       ignore_difficult_instances,
                       image_subdirectory):
  """Convert XML derived dict to tf.Example proto.

  Notice that this function normalizes the bounding box coordinates provided
  by the raw data.

  Args:
    data: dict holding PASCAL XML fields for a single image (obtained by
      running dataset_util.recursive_parse_xml_to_dict)
    dataset_directory: Path to root directory holding PASCAL dataset
    label_map_dict: A map from string label names to integers ids.
    ignore_difficult_instances: Whether to skip difficult instances in the
      dataset  (default: False).
    image_subdirectory: String specifying subdirectory within the
      PASCAL dataset directory holding the actual image data.

  Returns:
    example: The converted tf.Example.

  Raises:
    ValueError: if the image pointed to by data['filename'] is not a valid JPEG
  """
  img_path = JoinPath(dataset_directory,data['folder'], image_subdirectory, data['filename'])
  #print(img_path)
  full_path = JoinPath(dataset_directory, img_path)
  #print(full_path)
  with tf.gfile.GFile(full_path, 'rb') as fid:
    encoded_jpg = fid.read()
  encoded_jpg_io = io.BytesIO(encoded_jpg)
  image = PIL.Image.open(encoded_jpg_io)
  #if image.format != 'JPEG':
    #raise ValueError('Image format not JPEG')
  key = hashlib.sha256(encoded_jpg).hexdigest()

  width = int(data['size']['width'])
  height = int(data['size']['height'])

  xmin = []
  ymin = []
  xmax = []
  ymax = []
  classes = []
  classes_text = []
  truncated = []
  poses = []
  difficult_obj = []
  if 'object' in data:
    for obj in data['object']:
      difficult = bool(int(obj['difficult']))
      if ignore_difficult_instances and difficult:
        continue

      difficult_obj.append(int(difficult))

      xmin.append(float(obj['bndbox']['xmin']) / width)
      ymin.append(float(obj['bndbox']['ymin']) / height)
      xmax.append(float(obj['bndbox']['xmax']) / width)
      ymax.append(float(obj['bndbox']['ymax']) / height)
      classes_text.append(obj['name'].encode('utf8'))
      #print(label_map_dict)
      #print(label_map_dict[obj['name']])
      classes.append(label_map_dict[obj['name']])
      truncated.append(int(obj['truncated']))
      poses.append(obj['pose'].encode('utf8'))

  example = tf.train.Example(features=tf.train.Features(feature={
      'image/height': dataset_util.int64_feature(height),
      'image/width': dataset_util.int64_feature(width),
      'image/filename': dataset_util.bytes_feature(
          data['filename'].encode('utf8')),
      'image/source_id': dataset_util.bytes_feature(
          data['filename'].encode('utf8')),
      'image/key/sha256': dataset_util.bytes_feature(key.encode('utf8')),
      'image/encoded': dataset_util.bytes_feature(encoded_jpg),
      'image/format': dataset_util.bytes_feature('jpeg'.encode('utf8')),
      'image/object/bbox/xmin': dataset_util.float_list_feature(xmin),
      'image/object/bbox/xmax': dataset_util.float_list_feature(xmax),
      'image/object/bbox/ymin': dataset_util.float_list_feature(ymin),
      'image/object/bbox/ymax': dataset_util.float_list_feature(ymax),
      'image/object/class/text': dataset_util.bytes_list_feature(classes_text),
      'image/object/class/label': dataset_util.int64_list_feature(classes),
      'image/object/difficult': dataset_util.int64_list_feature(difficult_obj),
      'image/object/truncated': dataset_util.int64_list_feature(truncated),
      'image/object/view': dataset_util.bytes_list_feature(poses),
  }))
  return example


def main(directory,record_name):
  try:
    

    data_dir = directory #FLAGS.data_dir
    #years = ['VOC2007', 'VOC2012']
    

    label_map_path=JoinPath(directory,'pascal_label_map.pbtxt')
    output_path=JoinPath(directory,record_name)
    '''
    Year='VOC2012'
    

    if FLAGS.set not in SETS:
      raise ValueError('set must be in : {}'.format(SETS))
    if Year not in YEARS:
      raise ValueError('year must be in : {}'.format(YEARS))
    '''

    writer = tf.python_io.TFRecordWriter(output_path)
    
    label_map_dict = label_map_util.get_label_map_dict(label_map_path)

    #for year in years:
      #logging.info('Reading from PASCAL %s dataset.', year)

    #print(data_dir)
    #examples_path = JoinPath(data_dir,'ImageSets', 'Main','BSP-CLEAN' + FLAGS.set +'.txt')
    examples_path = JoinPath(data_dir,'ImageSets','Main')
    images_list=[]
    for txt in os.listdir(examples_path):
        #examples_list = dataset_util.read_examples_list(JoinPath(examples_path,txt))
        images_list=images_list+dataset_util.read_examples_list(JoinPath(examples_path,txt))

    #examples_path = JoinPath(data_dir,'ImageSets', 'Main',example_tag_file + '.txt')

    seen = set()
    uniq = [x for x in images_list if x not in seen and not seen.add(x)]
    
    import collections
    re_1=[item for item, count in collections.Counter(images_list).items() if count > 1]
    #print(len(examples_list),len(images_list),len(uniq),len(re_1))

    
    #annotations_dir = JoinPath(data_dir, FLAGS.annotations_dir)
    annotations_dir = JoinPath(data_dir,'Annotation')
    #examples_list = dataset_util.read_examples_list(examples_path)
    #print(len(uniq))
    #print(len(images_list))
    for idx, example in enumerate(uniq):
      try:
        if idx % 100 == 0:
          logging.info('On image %d of %d', idx, len(uniq))
          #pass

        path = JoinPath(annotations_dir, os.path.splitext(example)[0]+ '.xml')
        #print(path)
       
        with tf.gfile.GFile(path, 'r') as fid:

          xml_str = fid.read()
          #print(xml_str)
        with open(JoinPath(path)) as f:
          #print(path)
          tree = ET.parse(f)
          
          root = tree.getroot()

        xml_str=ET.tostring(root,encoding='utf8',method='html')

        xml = etree.fromstring(xml_str)
        
        data = dataset_util.recursive_parse_xml_to_dict(xml)['annotation']

        tf_example = dict_to_tf_example(data, directory, label_map_dict,False,'JPEGImages')
        #print(tf_example)
        writer.write(tf_example.SerializeToString())
      except Exception as msg:
        #traceback.print_exc()
        print(msg)
        #pass
      
        
        
    writer.close()
    
    return output_path , len(uniq)
    #print(output_path)
  except Exception as msg:
    print(msg)
    #pass


if __name__ == '__main__':
  main('/home/gaming/plant_vott/target_plant/plant-PascalVOC-export','TRAIN.record')  
  #main('/home/gaming/Downloads/Segregation/tf_1_training/Batch5_turf_area_training/Target/TEST','TEST.record')  
  #tf.app.run()

