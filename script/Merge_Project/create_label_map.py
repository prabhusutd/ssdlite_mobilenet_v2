from object_detection.protos.string_int_label_map_pb2 import StringIntLabelMap, StringIntLabelMapItem
from google.protobuf import text_format
import os


def convert_classes(classes, start=1):
    '''
        description : convert list of names to pbtxt with id
        format : {id:1 item:cat}
        input : list_of_classes
    '''
    msg = StringIntLabelMap()
    for id, name in enumerate(classes, start=start):
        msg.item.append(StringIntLabelMapItem(id=id, name=name))

    text = str(text_format.MessageToBytes(msg, as_utf8=True), 'utf-8')
    return text

def main(class_list,path):
	'''
		description: store pbtxt format as a pbtxt file
		input : pbtxt path , class list
	'''
	try:
	    txt = convert_classes(class_list)
	    filepath=os.path.join(path,'pascal_label_map.pbtxt')
	    with open(filepath, 'w') as f:
	        f.write(txt)

	    return filepath
	except Exception as msg:
		print(msg)
		return False


if __name__ == '__main__':
    txt = convert_classes(['cat', 'dog', 'fox', 'squirrel'])
    print(txt)
    with open('/home/gaming/Downloads/Segregation/check/Batch1_turf_area_training/label_map.pbtxt', 'w') as f:
        f.write(txt)