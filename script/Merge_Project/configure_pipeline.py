import re


def main(**ConfigParameters):
	'''
	description: change configuation parameters in pipeline config file
	input: source and destionation path of files and confiuration parameters
	'''
	try:
		with open(ConfigParameters['src_pipeline_fname']) as f:
			s = f.read()

		with open(ConfigParameters['dest_pipeline_fname'], 'w') as f:

			fine_tune_checkpoint='/content/models/research/pretrained_models/ssdlite_mobilenet_v2_coco_2018_05_09/model.ckpt'
			# fine_tune_checkpoint
			s=re.sub('fine_tune_checkpoint: ".*?"','fine_tune_checkpoint: "{}"'.format(fine_tune_checkpoint), s)
			# tfrecord files train and test.
			s = re.sub('(input_path: ".*?)(TRAIN.record)(.*?")', 'input_path: "{}"'.format(ConfigParameters['train_record_fname']), s)
			s = re.sub('(input_path: ".*?)(TEST.record)(.*?")', 'input_path: "{}"'.format(ConfigParameters['test_record_fname']), s)
			# label_map_path
			s = re.sub('label_map_path: ".*?"', 'label_map_path: "{}"'.format(ConfigParameters['label_map_pbtxt_fname']), s)
			# Set training batch_size.
			s = re.sub('batch_size: [0-9]+','batch_size: {}'.format(ConfigParameters['batch_size']), s)
			# Set training steps, num_steps
			s = re.sub('num_steps: [0-9]+','num_steps: {}'.format(ConfigParameters['num_steps']), s)
			# Set number of classes num_classes.
			s = re.sub('num_classes: [0-9]+','num_classes: {}'.format(ConfigParameters['num_classes']), s)
			#set number of examples in eval
			s = re.sub('num_examples: [0-9]+','num_examples: {}'.format(ConfigParameters['num_examples']), s)
			f.write(s)
			#print(s)
		return True
	except Exception as msg:
		print(msg)
		return False

if __name__ == "__main__":
	main(**{'src_pipeline_fname':'/home/gaming/DIC_training/Tensorflow_1/scripts/Merge_Project/pipeline.config',
                                'dest_pipeline_fname':'/home/gaming/DIC_training/Tensorflow_1/scripts/Merge_Project/pipeline.config',
                                'label_map_pbtxt_fname':'/home/gaming/Downloads/Segregation/check/Batch1_turf_area_training/Target/TRAIN/pascal_label_map.pbtxt',
                                'train_record_fname':'/home/gaming/Downloads/Segregation/check/Batch1_turf_area_training/Target/TRAIN/TRAIN.record',
                                'test_record_fname':'/home/gaming/Downloads/Segregation/check/Batch1_turf_area_training/Target/TEST/TEST.record',
                                'batch_size':34,'num_steps':100000,
                                'num_classes':10,'num_examples':1000})
	
